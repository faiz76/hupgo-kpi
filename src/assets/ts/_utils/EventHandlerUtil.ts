import {DataUtil} from './_DataUtil'
import {getUniqueIdWithPrefix} from './_TypesHelpers'

export interface EventMeta {
  name: string
  callback: Function
  one: boolean
  fired: boolean
}

export class EventHandlerUtil {
  static store: Map<string, Map<string, EventMeta>> = new Map()

  private static setEventMetasByName(name: string, metas: Map<string, EventMeta>): void {
    EventHandlerUtil.store.set(name, metas)
  }

  private static getEventMetasByName(name: string): Map<string, EventMeta> | undefined {
    return EventHandlerUtil.store.get(name)
  }

  private static setEventMetaByNameAndHandlerId(
    name: string,
    handlerId: string,
    meta: EventMeta
  ): void {
    let metas = EventHandlerUtil.getEventMetasByName(name)
    if (!metas) {
      metas = new Map()
    }

    metas.set(handlerId, meta)
    EventHandlerUtil.setEventMetasByName(name, metas)
  }

  private static getEventsMetaByHandlerId(name: string, handlerId: string): EventMeta | undefined {
    const metas = EventHandlerUtil.store.get(name)
    if (!metas) {
      return
    }

    return metas.get(handlerId)
  }

  private static setFiredByNameAndHandlerId(name: string, handerId: string, fired: boolean): void {
    const meta = EventHandlerUtil.getEventsMetaByHandlerId(name, handerId)
    if (!meta) {
      return
    }

    meta.fired = fired
    EventHandlerUtil.setEventMetaByNameAndHandlerId(name, handerId, meta)
  }

  private static addEvent(
    element: HTMLElement,
    name: string,
    callback: Function,
    one: boolean = false
  ) {
    const handlerId = getUniqueIdWithPrefix('event')
    DataUtil.set(element, name, handlerId)
    const meta: EventMeta = {
      name: name,
      callback: callback,
      one: one,
      fired: false,
    }

    EventHandlerUtil.setEventMetaByNameAndHandlerId(name, handlerId, meta)
  }

  private static removeEvent(element: HTMLElement, name: string, handerId: string) {
    DataUtil.removeOne(element, name, handerId)
    const handlersIds = EventHandlerUtil.store[name]
    if (handlersIds) {
      return
    }

    delete EventHandlerUtil.store[name][handerId]
  }

  public static trigger(element: HTMLElement, name: string, target?: any, e?: Event): boolean {
    let returnValue = true
    if (!DataUtil.has(element, name)) {
      return returnValue
    }

    let eventValue
    let handlerId
    const data = DataUtil.get(element, name)
    const handlersIds = data ? (data as string[]) : []
    for (let i = 0; i < handlersIds.length; i++) {
      handlerId = handlersIds[i]
      if (EventHandlerUtil.store[name] && EventHandlerUtil.store[name][handlerId]) {
        const handler = EventHandlerUtil.store[name][handlerId]
        if (handler.name === name) {
          if (handler.one) {
            if (handler.fired) {
              EventHandlerUtil.store[name][handlerId].fired = true
              eventValue = handler.callback.call(this, target)
            }
          } else {
            eventValue = handler.callback.call(this, target)
          }

          if (eventValue === false) {
            returnValue = false
          }
        }
      }
    }
    return returnValue
  }

  public static on = function (element: HTMLElement, name: string, callBack: Function): void {
    EventHandlerUtil.addEvent(element, name, callBack, false)
  }

  public static one(element: HTMLElement, name: string, callBack: Function): void {
    EventHandlerUtil.addEvent(element, name, callBack, true)
  }

  public static off(element: HTMLElement, name: string, handerId: string): void {
    EventHandlerUtil.removeEvent(element, name, handerId)
  }
}
