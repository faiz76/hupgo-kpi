import axios from 'axios'
import { cacheAdapterEnhancer } from 'axios-extensions';

window.axios = axios
// axios.defaults.withCredentials = true
axios.defaults.baseURL = process.env.VUE_APP_API_URL
axios.defaults.adapter = cacheAdapterEnhancer(axios.defaults.adapter, { enabledByDefault: false, cacheFlag: 'useCache'})
axios.interceptors.request.use( config => {
    config.headers.common['Cache-Control'] = 'max-age=604800, must-revalidate',
    config.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token'),
    config.headers.common['Content-Type'] = 'application/json',
    config.headers.common['Accept'] = 'application/json'    
    return config
})

