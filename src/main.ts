import { createApp } from "vue"
import App from "./App.vue"

/*
TIP: To get started with clean router change path to @/router/clean.ts.
 */
import router from "@/router/routes"
import store from "./store"
import ElementPlus from "element-plus"
import '@/axios'

//imports for app initialization
import vSelect from "vue-select"
import VueExcelXlsx from "vue-excel-xlsx"
import Multiselect from "@vueform/multiselect"
import VueApexCharts from "vue3-apexcharts";
import VLazyImage from "v-lazy-image";

import { initInlineSvg } from "@/core/plugins/inline-svg"
import { initVeeValidate } from "@/core/plugins/vee-validate"

import "@/core/plugins/prismjs"
import "bootstrap"

const app = createApp(App)

app.use(store)
app.use(router)
app.use(ElementPlus)
app.use(VueExcelXlsx)

app.component("v-lazy-image", VLazyImage)
app.component("v-select", vSelect)
app.component("Multiselect", Multiselect)
app.component("apexchart", VueApexCharts);

initInlineSvg(app)
initVeeValidate()

app.mount("#app")
