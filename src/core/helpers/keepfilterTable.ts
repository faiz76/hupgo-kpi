const setFilter = (params, value) => {
    localStorage.setItem(params, value)
}

const getFilter = (params) => {
    return localStorage.getItem(params)
}

export { setFilter, getFilter };