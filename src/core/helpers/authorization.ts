const teamsAuthorization = (match) => {
  let localS = localStorage.getItem("teams" || "{}");

  if (localS) {
    return localS.includes(match) ? true : false;
  }

}

const subModulAuthorization = (team, modul) => {
  let teams = JSON.parse(localStorage.getItem("teams") || "{}");
  if (modul !== undefined) {
    return modul.includes(teams[team]) ? true : false;
  }

  return true;
}

export { teamsAuthorization, subModulAuthorization };
