import * as am4core from "@amcharts/amcharts4/core";

var indicator;
var indicatorInterval;

const showIndicator = (chart) => {
    indicator = chart.tooltipContainer.createChild(am4core.Container);
    indicator.background.fill = am4core.color("#fff");
    indicator.background.fillOpacity = 0.8;
    indicator.width = am4core.percent(100);
    indicator.height = am4core.percent(100);

    indicator.hide(0);
    indicator.show();

    var indicatorLabel = indicator.createChild(am4core.Label);
    indicatorLabel.text = "Loading ...";
    indicatorLabel.align = "center";
    indicatorLabel.valign = "middle";
    indicatorLabel.fontSize = 16;
    indicatorLabel.dy = 50;

    var hourglass = indicator.createChild(am4core.Image);
    hourglass.href = "/media/svg/logos/hupgo.svg";
    hourglass.align = "center";
    hourglass.valign = "middle";
    hourglass.horizontalCenter = "middle";
    hourglass.verticalCenter = "middle";
    hourglass.scale = 0.7;

    clearInterval(indicatorInterval);
    indicatorInterval = setInterval(function () {
        hourglass.animate(
            [
                {
                    from: 0,
                    to: 360,
                    property: "rotation",
                },
            ],
            2000
        );
    }, 3000);
};

const hideIndicator = () => {
    indicator.hide();
    clearInterval(indicatorInterval);
};

export { showIndicator, hideIndicator };
