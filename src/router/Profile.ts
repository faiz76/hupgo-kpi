export default [
    {
        path: "user-profile",
        name: "account",
        component: () => import("@/views/UserProfile/Profile.vue"),
    }
]