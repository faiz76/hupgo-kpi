export default [
    {
        path: "/kpi/master-perspective-statis",
        name: "master-perspective-statis",
        component: () => import("@/views/KPI/Master/PerspectiveStatis/index.vue"),
    },
    {
        path: "/kpi/master-perspective-statis/create",
        name: "master-perspective-statis-add",
        component: () => import("@/views/KPI/Master/PerspectiveStatis/create.vue"),
    },
    {
        path: "/kpi/master-perspective-statis/edit/:id",
        name: "master-perspective-statis-edit",
        component: () => import("@/views/KPI/Master/PerspectiveStatis/edit.vue"),
    },
    {
        path: "/kpi/master-perspective-dinamis",
        name: "master-perspective-dinamis",
        component: () => import("@/views/KPI/Master/PerspectiveDinamis/index.vue"),
    },
    {
        path: "/kpi/master-perspective-dinamis/create",
        name: "master-perspective-dinamis-add",
        component: () => import("@/views/KPI/Master/PerspectiveDinamis/create.vue"),
    },
    {
        path: "/kpi/master-perspective-dinamis/edit/:id",
        name: "master-perspective-dinamis-edit",
        component: () => import("@/views/KPI/Master/PerspectiveDinamis/edit.vue"),
    },
    {
        path: "/kpi/master-category-formula",
        name: "master-category-formula",
        component: () => import("@/views/KPI/Master/CategoryFormula/index.vue"),
    },
    {
        path: "/kpi/master-category-formula/create",
        name: "master-category-formula-add",
        component: () => import("@/views/KPI/Master/CategoryFormula/create.vue"),
    },
    {
        path: "/kpi/master-category-formula/edit/:id",
        name: "master-category-formula-edit",
        component: () => import("@/views/KPI/Master/CategoryFormula/edit.vue"),
    },
    {
        path: "/kpi/list-data-kpi",
        name: "list-kpi",
        component: () => import("@/views/KPI/Transaksi/InputDataKPI/index.vue"),
    },
    {
        path: "/kpi/create-data-kpi",
        name: "create-kpi",
        component: () => import("@/views/KPI/Transaksi/InputDataKPI/create.vue"),
    },
    {
        path: "/kpi/edit-data-kpi/:id",
        name: "edit-kpi",
        component: () => import("@/views/KPI/Transaksi/InputDataKPI/edit.vue"),
    },
    {
        path: "/kpi/kpi-focus",
        name: "kpi-focus",
        component: () => import("@/views/KPI/Transaksi/Focus.vue")
    },
    {
        path: "/kpi/kpi-perpective",
        name: "kpi-perspective",
        component: () => import("@/views/KPI/Transaksi/Perspective.vue")
    }
]