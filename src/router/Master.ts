export default [
    {
        path: "/kpi/master/cluster",
        name: "master-master-cluster",
        component: () => import("@/views/Master/Cluster/index.vue")
    },
    {
        path: "/kpi/master/cluster/add",
        name: "master-master-cluster-add",
        component: () => import("@/views/Master/Cluster/create.vue")
    },
    {
        path: "/kpi/master/cluster/edit/:id",
        name: "master-master-cluster-edit",
        component: () => import("@/views/Master/Cluster/edit.vue")
    },
    {
        path: "/kpi/master/site",
        name: "master-master-site",
        component: () => import("@/views/Master/Site/index.vue")
    },
    {
        path: "/kpi/master/site/add",
        name: "master-master-site-add",
        component: () => import("@/views/Master/Site/create.vue")
    },
    {
        path: "/kpi/master/site/:id",
        name: "master-master-site-edit",
        component: () => import("@/views/Master/Site/edit.vue")
    },
]