import axios from "axios";
import Swal from "sweetalert2/dist/sweetalert2.min.js";
import { createRouter, createWebHistory, type RouteRecordRaw } from "vue-router";

import Profile from "./Profile";
import Overview from "./Overview";
import KPI from "./KPI";
import Master from "./Master";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/homedashboard",
    component: () => import("@/layout/Layout.vue"),
    meta: {
      middleware: "auth",
    },
    children: [
      ...Profile,
      ...Overview,
      ...KPI,
      ...Master
    ],
  },
  {
    path: "/",
    component: () => import("@/components/page-layouts/Auth.vue"),
    children: [
      {
        path: "sign-in",
        name: "sign-in",
        component: () =>
          import("@/views/Auth/SignIn.vue"),
      },
      {
        path: "password-reset",
        name: "password-reset",
        component: () =>
          import("@/views/Auth/PasswordReset.vue"),
      },
      {
        path: "password-reset-verification",
        name: "password-reset-verification",
        component: () =>
          import("@/views/Auth/PasswordResetVerification.vue"),
      },
    ],
  },
  {
    // the 404 route, when none of the above matches
    path: "/404",
    name: "404",
    component: () => import("@/views/Auth/Error404.vue"),
  },
  {
    path: "/500",
    name: "500",
    component: () => import("@/views/Auth/Error500.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: "/404",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

const responseSuccessHandler = response => {
  return response;
};

const responseErrorHandler = error => {

  if (error.response != null) {
    if (error.response.status === 401) {
      Swal.fire({
        title: "Oops! Something went wrong",
        text: error.response.data.message,
        icon: "warning",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      }).then(() => {
        localStorage.clear();
        router.push({ name: "sign-in" });
      })
    }

    if (error.response.status === 403) {
      Swal.fire({
        title: "Oops! Something went wrong",
        text: error.response.data.message,
        icon: "warning",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }

    if (error.response.status === 420) {
      Swal.fire({
        title: "Oops, Forbidden",
        text: "You are not authorized to access this page. Please contact your administrator.",
        icon: "warning",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      }).then(() => {
        router.push({ name: "homedashboard" });
      })
    }

    if (error.response.status === 429) {
      Swal.fire({
        title: "To Many Attempts",
        text: "The given data was invalid. Please wait before retrying.",
        icon: "warning",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      }).then(() => {
        router.push({ name: "password-reset" });
      })
    }

    if (error.response.status === 500) {
      Swal.fire({
        title: "Oops! Something went wrong, please contact your administrator",
        text: error.response.data.message,
        icon: "warning",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn fw-bold btn-light-primary",
        },
      })
    }
  }

  return Promise.reject(error);
};

axios.interceptors.response.use(
  response => responseSuccessHandler(response),
  error => responseErrorHandler(error)
);

router.beforeEach((to, from, next) => {

  let token = localStorage.getItem("token");

  if (to.meta.middleware == "auth") {
    if (token) {
      next();
    } else {
      next({ name: "sign-in" });
    }
  } else {
    next();
  }

  // Scroll page to top on every route change
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: "smooth",
  });
});

export default router;
