import KPI from "./KPI";
import master from "./master";

const HupgoMenuConfig = [
  {
    heading: "Dashboard",
    route: "/",
    pages: [
      {
        heading: "Home Dashboard",
        route: "/homedashboard",
        svgIcon: "/media/icons/duotune/general/gen008.svg",
        fontIcon: "bi-app-indicator",
      },
      {
        heading: "User Profile",
        route: "/user-profile",
        svgIcon: "/media/icons/duotune/technology/teh002.svg",
        fontIcon: "bi-app-indicator",
      }
    ],
  },
  {
    heading: "APP",
    route: "/APP",
    pages: [
      ... master,
      ... KPI,
    ]
  },
];

export default HupgoMenuConfig;