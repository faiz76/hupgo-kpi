export default [
    {
        sectionTitle: "KPI",
        svgIcon: "/media/icons/duotune/general/gen028.svg",
        fontIcon: "bi-archive",
        sub: [
          {
            sectionTitle: "Master",
            sub: [
              {
                heading: "Perspective (Statis)",
                route: "/kpi/master-perspective-statis",
              },   
              {
                heading: "Perspective (Dinamis)",
                route: "/kpi/master-perspective-dinamis",
              },              
              {
                heading: "Category & Formula",
                route: "/kpi/master-category-formula",
              },       
            ]
          },
          {
            sectionTitle: "Transaction",
            sub: [
              {
                heading: "Input Data KPI",
                route: "/kpi/list-data-kpi",
              }
            ]
          },
        ]
      },
]