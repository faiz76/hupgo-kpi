export default [
    {
        sectionTitle: "Data Master",
        svgIcon: "/media/icons/duotune/general/gen001.svg",
        fontIcon: "bi-archive",
        sub: [
          {
            heading: "Cluster Data",
            route: "/kpi/master/cluster"
          },
          {
            heading: "Site Data",
            route: "/kpi/master/site"
          },
        ]
      },
]