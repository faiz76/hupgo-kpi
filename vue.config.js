const { defineConfig } = require("@vue/cli-service");
const TerserPlugin = require('terser-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const path = require('path');
const zlib = require("zlib");
const webpack = require('webpack')
const dotenv = require('dotenv').config()

module.exports = defineConfig({
  publicPath: '/',
  transpileDependencies: true,
  lintOnSave: false,
  configureWebpack: {
    mode: process.env.NODE_ENV,
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        'components': path.resolve(__dirname, 'src/components/'),
      },
    },
    optimization: {
      minimize: true,
      minimizer: [new TerserPlugin()],
      splitChunks: {
        chunks: 'all',
        minSize: 20000,
        maxSize: 50000,
        minChunks: 1,
        maxAsyncRequests: 30,
        maxInitialRequests: 30,
        automaticNameDelimiter: '~',
        enforceSizeThreshold: 50000,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true,
          },
        },
      },
    },
    plugins: [
      new NodePolyfillPlugin(),
      new webpack.DefinePlugin({
        'process.env': dotenv.parsed
      }),
      new CompressionWebpackPlugin({
        test: /\.(js|css|html|svg)$/,
        algorithm: 'gzip',
        compressionOptions: { level: 9 },
        minRatio: 0.8
      }),
      // new BundleAnalyzerPlugin(),
    ],
    externals: ({ context, request }, callback) => {
      if (/canvg|pdfmake/.test(request)) {
        callback(null, 'commonjs ' + request);
      } else {
        callback();
      }
    },
  },
  pluginOptions: {
    compression: {
      brotli: {
        filename: '[file].br[query]',
        algorithm: 'brotliCompress',
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        compressionOptions: {
          params: {
            [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
          },
        },
        minRatio: 0.8,
      },
      gzip: {
        filename: '[file].gz[query]',
        algorithm: 'gzip',
        include: /\.(js|css|html|svg|json)(\?.*)?$/i,
        minRatio: 0.8,
      }
    }
  },
  chainWebpack: config => {
    config
      .module
      .rule('pdfmake')
      .test(/pdfmake.js$/)
      .use('null-loader')
      .loader('null-loader')
      .end()
      .rule('xlsx')
      .test(/xlsx.js$/)
      .use('null-loader')
      .loader('null-loader')
      .end()
      .rule('canvg')
      .test(/canvg.js$/)
      .use('null-loader')
      .loader('null-loader')
      .end()
      .rule('svg')
      .test(/\.svg$/)
      .use('null-loader')
      .end()
      .rule('images')
      .set('parser', {
        dataUrlCondition: {
          maxSize: 4 * 1024 // 4KiB
        }
      })
    config.plugin('vue-loader').tap((options) => {
      options.compilerOptions = {
        ...(options.compilerOptions || {}),
        isCustomElement: (tag) => tag.startsWith('my-'),
        // enable tree shaking
        compilerOptions: {
          modules: [
            {
              // enable tree shaking for custom elements
              preTransformNode(node, options) {
                if (node.tag === 'my-custom-element') {
                  node.tag = 'div';
                }
              },
            },
          ],
        },
      };
      return options;
    });
  }
});
